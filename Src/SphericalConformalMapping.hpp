#ifndef _CONFORMAL_EMBEDDING_H_
#define _CONFORMAL_EMBEDDING_H_

#include "Point.h"
#include "Solid.h"
#include "SolidDelegate.h"

using namespace MeshLib;

class ConformalEmbedding
{
public:
    ConformalEmbedding(Solid &m)
    {
        energyThreshold = 0.00001; // 1e-5
        stepSize = 0.005;
        mesh = &m;
    }
    void computeGaussMap()
    {
        mesh->UpdateNormals();
        SolidVertexIterator svIter(mesh);
        // 1. Creating the Gaussian Map:
        // 2. Initializing the sphere with the normal as points and creating faces
        for (svIter.reset(); !svIter.end(); ++svIter)
        {
            Vertex *v = *svIter;
            Point normal = v->normal();
            Vertex *v2 = delegate.createVertex(&sph_mesh,
                                               sph_mesh.numVertices() + 1);
            v2->point() = normal;
            v2->id() = v->id();
        }
        SolidFaceIterator f_iter(mesh);
        for (f_iter.reset(); !f_iter.end(); ++f_iter)
        {
            Face *f = *f_iter;
            HalfEdge *he = f->halfedge();
            int faces[3] = {he->source()->id(),
                            he->target()->id(),
                            he->he_next()->target()->id()};
            delegate.createFace(&sph_mesh, faces, sph_mesh.numFaces() + 1);
        }
        // 3. Gauss Map  energy
        energy_gauss = 0;
        SolidEdgeIterator edge_iter(&sph_mesh);
        for (edge_iter.reset(); !edge_iter.end(); ++edge_iter)
        {
            Edge *e = *edge_iter;
            Vertex *v1, *v2;
            e->get_vertices(v1, v2);
            energy_gauss = energy_gauss + (v1->point() - v2->point()).norm2();
        }
        std::cout << "Gauss Energy = " << energy_gauss << "\n";
    };

    void getLaplacian(Vertex *v_old, Point &laplacian)
    {
        Vertex *newv = sph_mesh.idVertex(v_old->id());
        // TODO: var
        for (VertexVertexIterator vv_iter(v_old); !vv_iter.end(); ++vv_iter)
        {
            Vertex *vv = *vv_iter;
            Vertex *newvv = sph_mesh.idVertex(vv->id());
            laplacian += (newvv->point() - newv->point());
        }
        laplacian /= laplacian.norm();
    }

    void computeTuetteEnergy()
    {
        double energy_total = 0.0, energy_cache = energy_gauss;
        while (1)
        {
            SolidVertexIterator iter_v(&sph_mesh);
            for (iter_v.reset(); !iter_v.end(); ++iter_v)
            {
                Vertex *v = *iter_v;
                Point laplacian;
                getLaplacian(v, laplacian);
                Point diff = -(laplacian - (v->point() * (laplacian * v->point()))) * stepSize;
                v->point() -= diff;
                v->point() /= v->point().norm();
            }
            energy_total = 0.0;
            for (SolidEdgeIterator e1_iter(&sph_mesh); !e1_iter.end(); ++e1_iter)
            {
                Edge *e = *e1_iter;
                Vertex *v1, *v2;
                e->get_vertices(v1, v2);
                energy_total = energy_total + (v1->point() - v2->point()).norm2();
            }
            std::cout << "Tuette energy is :" << energy_total
                      << "Energy diff = " << energy_total - energy_cache << "\n";
            if (fabs(energy_total - energy_cache) < energyThreshold)
            {
                energy_gauss = energy_total;
                break;
            }
            energy_cache = energy_total;
        }
    };

    double _computeStringConstant(Edge *edge)
    {
        Point p1 = edge->halfedge(0)->source()->point();
        Point p2 = edge->halfedge(0)->target()->point();
        Point p3 = edge->halfedge(0)->he_next()->target()->point();
        Point p4 = edge->halfedge(1)->he_next()->target()->point();
        double alpha = ((p1 - p3) * (p2 - p3)) / (((p1 - p3) ^ (p2 - p3)).norm());
        double beta = ((p1 - p4) * (p2 - p4)) / (((p1 - p4) ^ (p2 - p4)).norm());
        return 0.5 * (alpha + beta);
    };

    double getAllStringConstants()
    {
        for (SolidEdgeIterator e_kuv(mesh); !e_kuv.end(); ++e_kuv)
        {
            Edge *e = *e_kuv;
            string_constants[e] = _computeStringConstant(e);
        }
    };

    Solid *minimizeHarmonicEnergy()
    {
        std::cout << "-----Harmonic energy minimization-----\n";
        double energy_total = 0.0, energy_cache = energy_gauss;
        while (1)
        {
            SolidVertexIterator iter_v(&sph_mesh);
            for (iter_v.reset(); !iter_v.end(); ++iter_v)
            {
                Vertex *v = *iter_v;
                Point laplacian;
                getLaplacian(v, laplacian);
                Point diff = -(laplacian - (v->point() * (laplacian * v->point()))) * stepSize;
                v->point() -= diff;
                v->point() /= v->point().norm(); // see if this normalisation is needed here
            }

            Point center;
            for (SolidVertexIterator iter_centre(&sph_mesh); !iter_centre.end(); ++iter_centre)
            {
                Vertex *v = *iter_centre;
                double vertex_face_area = 0.0;
                int count = 0;
                for (MeshLib::VertexFaceIterator vfiter(v); !vfiter.end(); ++vfiter)
                {
                    MeshLib::Face *f = *vfiter;
                    Point p1 = f->halfedge()->source()->point();
                    Point p2 = f->halfedge()->target()->point();
                    Point p3 = f->halfedge()->he_next()->target()->point();
                    vertex_face_area += (0.5 * (((p1 - p2) ^ (p1 - p3)).norm()));
                    count++;
                }
                vertex_face_area /= count;
                //std::cout << "vertex_face_area = " << vertex_face_area << "\n";
                center = center + v->point() * vertex_face_area;
            }
            center = center / sph_mesh.numVertices();
            // recenter the points
            for (SolidVertexIterator pv(&sph_mesh); !pv.end(); ++pv)
            {
                Vertex *pV = *pv;
                pV->point() -= center;
                pV->point() /= pV->point().norm();
            }

            // Calculate the Harmonic Energy
            energy_total = 0.0;
            for (SolidEdgeIterator e1_iter(&sph_mesh); !e1_iter.end(); ++e1_iter)
            {
                Edge *e = *e1_iter;
                Vertex *v1, *v2;
                e->get_vertices(v1, v2);
                Vertex *v1_orig = mesh->idVertex(v1->id());
                Vertex *v2_orig = mesh->idVertex(v2->id());
                energy_total += ((v1->point() - v2->point()).norm2() *
                                 string_constants[mesh->vertexEdge(v1_orig, v2_orig)]); // harmonic Energy
            }
            // std::cout << "Harmoinc Energy is :" << energy_total
            //           << " Harmoinc Energy delta = " << energy_total - energy_cache << "\n";
            if (fabs(energy_total - energy_cache) < energyThreshold)
            {
                std::cout << "Harmoinc Energy is :" << energy_total
                          << " Harmoinc Energy delta = " << energy_total - energy_cache << "\n";
                energy_gauss = energy_total;
                energy_cache = energy_total;
                break;
            }
            energy_cache = energy_total;
        }
        return &sph_mesh ;
    };

private:
    Solid *mesh;
    Solid sph_mesh;
    double energyThreshold;
    double stepSize;
    SolidDelegate delegate;
    double energy_gauss = 0;
    std::map<Edge *, double> string_constants;
};

#endif //_TUETTE_EMBEDDING_H_