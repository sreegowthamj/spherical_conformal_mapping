#include <fstream>
#include <vector>

#include "OBJFileReader.h"
#include "Solid.h"
#include "iterators.h"
#include "SphericalConformalMapping.hpp"

using namespace MeshLib;

int main(int argc, char *argv[])
{
	// Read in the obj file
	Solid mesh, *output_mesh;
	OBJFileReader of;
	std::ifstream in(argv[1]);
	of.readToSolid(&mesh, in);

	/******************* Conformal Mapping Code here *********************/
	
	ConformalEmbedding tEmbed(mesh);
	// 1 : Computing Tuette Embedding
	// 1a. Compute gauss map
	tEmbed.computeGaussMap();
	// 1b. Compute Absolute derivative
	tEmbed.computeTuetteEnergy();
	tEmbed.getAllStringConstants();
	output_mesh = tEmbed.minimizeHarmonicEnergy();

	/*** End conformal mapping code ****/

	
	// Write out the resultant obj file
	int vObjID = 1;
	std::map<int, int> vidToObjID;

	std::ofstream os(argv[2]);

	SolidVertexIterator iter(output_mesh);

	for(; !iter.end(); ++iter)
	{
		Vertex *v = *iter;
		Point p = v->point();
		os << "v " << p[0] << " " << p[1] << " " << p[2] << std::endl;
		vidToObjID[v->id()] = vObjID++;
	}
	os << "# " << (unsigned int)output_mesh->numVertices() << " vertices" << std::endl;

	float u = 0.0, v = 0.0;
	for(iter.reset(); !iter.end(); ++iter)
	{
		Vertex *vv = *iter;
		std::string key( "uv" );
		std::string s = Trait::getTraitValue (vv->string(), key );
		if( s.length() > 0 )
		{
			sscanf( s.c_str (), "%f %f", &u, &v );
		}
		os << "vt " << u << " " << v << std::endl;
	}
	os << "# " << (unsigned int)output_mesh->numVertices() << " texture coordinates" << std::endl;

	SolidFaceIterator fiter(output_mesh);
	for(; !fiter.end(); ++fiter)
	{
		Face *f = *fiter;
		FaceVertexIterator viter(f);
		os << "f " ;
		for(; !viter.end(); ++viter)
		{
			Vertex *v = *viter;
			os << vidToObjID[v->id()] << "/" << vidToObjID[v->id()] << " ";
		}
		os << std::endl;
	}
	os.close();
	return 0;
}
